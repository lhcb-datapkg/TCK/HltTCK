TCK/HltTCK
======================================

How to add TCKs and tag
--------------------------------------
1. Clone the package
```sh
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/TCK/HltTCK.git && cd HltTCK
```
2. Copy the updated `config.cdb` file in the current directory
3. Upload the file to EOS with `make upload`, which uploads and modifies the
   link file `.config.cdb`. **Note:** only some people have permissions to do
   this step. If you are not among them, provide the `cdb` to someone who does.
4. Run `scripts/check-diffs` to make sure the CI will pass and copy the new TCKs
   for the release notes.
5. Edit `cmt/requirements` and update the version to the new version number.
6. Edit `doc/release.notes` and update that one with the relevant information
   and include a separator line for the new tag.
7. Commit the modified files with `git add -u` and `git commit`
8. Submit a MR to master, wait for the pipeline to finish sucessfully.
9. Once merged, create a [tag in GitLab](https://gitlab.cern.ch/lhcb-datapkg/TCK/HltTCK/tags/new).
