all: config.cdb config.tar

config.cdb: .config.cdb
config.tar: .config.tar
config.cdb config.tar:
	scripts/download-binary $@

manifest: config.cdb
manifest-tar: config.tar
manifest manifest-tar:
	source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh \
	&& lb-run Moore/v28r3 hlttck_cdb_listkeys.exe --list-manifest $< > $@

clean:
	rm -f config.cdb config.tar

upload:
	test -f "config.cdb" && scripts/upload-binary config.cdb || echo 'config.cdb does not exist, skipping'
	test -f "config.tar" && scripts/upload-binary config.tar || echo 'config.tar does not exist, skipping'

.PHONY: all clean upload
